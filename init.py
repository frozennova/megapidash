import serial, time, re, mserial, sys, signal,os
from datetime import datetime
from oled.serial import i2c
from oled.device import ssd1306, sh1106
from oled.render import canvas
from PIL import ImageFont, ImageDraw, Image
import RPi.GPIO as GPIO
#Setup OLED

# Configure GPIO
GPIO.setmode(GPIO.BCM)

abspath = os.path.abspath(__file__)
scriptdir = os.path.dirname(abspath)
logoImage = scriptdir + "/mx5.png"
fontFile = scriptdir + "/DroidSans.ttf"

#Setup OLED
seriali2c = i2c(port=1, address=0x3C)
device = sh1106(seriali2c)


with canvas(device) as draw:
	font = ImageFont.truetype(fontFile,22)
	messagetext = "Booting"
	size = draw.textsize(messagetext, font)
	draw.multiline_text((64-(size[0] / 2), 30), messagetext, font=font, fill="white", align="center")
    
time.sleep(10)

