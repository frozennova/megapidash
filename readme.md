Install dependencies

	sudo apt-get install python-dev
	sudo apt-get install libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk
	sudo pip install Pillow
	sudo pip install smbus2
    sudo pip install ssd1306


Create cronjob to launch on boot with crontab -e

	@reboot sh /home/pi/megapidash/launch.sh
