def formatEcuData( array, bit1,bit2, adder = 0, scale = 1 ):
	Hexbit = "0x"
	Hexbit = Hexbit +  "04" + "ab"
	out = s16(int(Hexbit + array[bit1] + array[bit2],16))
	out = (out + adder) * scale

	return round(out,3)
	
	
def calculateLph(RPM,injectorFlow,PW,DeadTime):
	lph = (((RPM/2)*(injectorFlow*(PW-DeadTime))*4)/1000)/600
	return lph;

def calculateDutyCycle(RPM,PW):
    if RPM > 0:
        CycleTime = 60000 / RPM * 2
    else:
        CycleTime = 0
    if PW > 1:
        DutyCycle = 100.0*2/2*PW/CycleTime
    else:
        DutyCycle = 0
    return DutyCycle;

def s16(value): return -(value & 0x8000) | (value & 0x7fff)