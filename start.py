import serial, time, re, mserial, sys, signal,os
from datetime import datetime
from oled.serial import i2c
from oled.device import ssd1306, sh1106
from oled.render import canvas
from PIL import ImageFont, ImageDraw, Image
import RPi.GPIO as GPIO
import threading
import Queue


abspath = os.path.abspath(__file__)
scriptdir = os.path.dirname(abspath)
logoImage = scriptdir + "/mx5.png"
fontFile = scriptdir + "/DroidSans.ttf"


exitFlag = 0
#while (time.time() < 1482950094 ):
#    pass
class FuncThread(threading.Thread):
    def __init__(self, target, *args):
        self._target = target
        self._args = args
        threading.Thread.__init__(self)
 
    def run(self):
        self._target(*self._args)

# Configure GPIO Buzzer pin 25, button pin 18
GPIO.setmode(GPIO.BCM)
GPIO.setup(25,GPIO.OUT)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
buzzer = GPIO.PWM(25,10)
buzzerStatus = 0
lastBuzz = 0				   
               
#setup keyboard interrupt handler
def signal_handler(signal, frame):
	print("\nUser aborted program")
	exitFlag = 1
	time.sleep(1)
	GPIO.cleanup()
	sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

#Setup Serial Connection
ser = serial.Serial()
ser.port = "/dev/ttyUSB0"
ser.baudrate = 115200
ser.bytesize = serial.EIGHTBITS #number of bits per bytes
ser.parity = serial.PARITY_NONE #set parity check: no parity
ser.stopbits = serial.STOPBITS_ONE #number of stop bits
ser.timeout = 1            #non-block read
ser.xonxoff = False     #disable software flow control
ser.rtscts = False     #disable hardware (RTS/CTS) flow control
ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control
ser.writeTimeout = 2     #timeout for write



#Setup variables
ecuData = {}
status = {}
data = {}
dataHeader = {}
injectorFlow = 650
injectorFlow = float(injectorFlow)
csvchar = ','
DeadTime = 0.9
DeadTimeCorrection = 0.1
status['connected'] = 0
status['currentPage'] = 0
status['error'] = 0
status['message'] = ""
status['lock'] = 0
degrees = "\xb0"
shiftRPM = 7100
shiftDrop = 200			   

limits = {}
limits['CLT'] = [98,"Coolant High"]

#Create Display headers
dataTitle = ["Average"]
dataTitle += ["Trip"]
dataTitle += ["Instant"]
dataTitle += ["Coolant"]
dataTitle += ["Inlet Temp"]
dataTitle += ["Boost"]
dataTitle += ["AFR"]
dataTitle += ["EGO"]
dataTitle += ["IDC"]
dataTitle += ["TPS"]
dataTitle += ["Battery Voltage"]

lastPage = len(dataTitle)
def buttonInput(channel): 
	global status 
	global lastPage 

	timer = time.time()

	while (GPIO.input(18) == 0):
		time.sleep(0.05)

	timer = time.time() - timer

	if ( timer <= 2.99  ):
		status['currentPage'] += 1 
		if (status['currentPage'] == lastPage):
			status['currentPage'] = 0 
		statusQ.put(status)
	else:
		ltc['distance'] = 0.001
		ltc['fuelUsed'] = 0.00000001

		status['error'] = 1
		status['lock'] = 1
		status['message'] = "Trip reset"
		statusQ.put(status)
		time.sleep(3)
		status['lock'] = 0
		status['message'] = ""
		status['error'] = 0

	statusQ.put(status)

#Start button press thread
GPIO.add_event_detect(18, GPIO.FALLING, callback=buttonInput,bouncetime=300)

# Start OLED Output threading

def oledOut(dataQ,statusQ,dataTitle,fontFile,logoImage):
	time.sleep(10)
	while(True):
		try:
			#Setup OLED
			seriali2c = i2c(port=1, address=0x3C)
			device = sh1106(seriali2c)
			break
		except:
			pass
	data = {}
	while not exitFlag:
		
		# Get Status Queue
	
		queueLock.acquire()
		if not statusQ.empty():
			#Get queue
			status = statusQ.get()
			currentPage = int(status['currentPage'])
			queueLock.release()
			
			#Print no connection to screen
			if (status['connected'] == 0):
				device.clear()
					
			if (status['error'] == 1 and status['connected'] == 1):
				with canvas(device) as draw:
					font = ImageFont.truetype(fontFile,22)
					messagetext = status['message']
					size = draw.textsize(messagetext, font)
					draw.multiline_text((64-(size[0] / 2), 30), messagetext, font=font, fill="white", align="center")
				
						

		else:
			queueLock.release()
		
		
		# Get Data Queue
		queueLock.acquire()
		if not dataQ.empty():
			data = dataQ.get()
			queueLock.release()
			if (status['error'] == 0):
				with canvas(device) as draw:
					font = ImageFont.truetype(fontFile,14)

					size = font.getsize(str(dataTitle[currentPage]))
					draw.text(( 64 - (size[0] / 2), 5), str(dataTitle[currentPage]), font=font, fill="white", align="center")

					font = ImageFont.truetype(fontFile,22)
					size = font.getsize(data[currentPage])
					draw.text((64-(size[0] / 2), 20), data[currentPage], font=font, fill="white", align="center")

					font = ImageFont.truetype(fontFile,10)
					draw.rectangle((6, 48, 118, 58), fill="white")

					if (status['engine'] & 8):
						font = ImageFont.truetype(fontFile,10)
						draw.text((8, 48), "WUE", font=font, fill="black", align="center")

					if (status['gpioport2'] & 4):
						draw.text((32, 48), "PUMP", font=font, fill="black", align="center")
					else:
						draw.text((32, 48), "METH", font=font, fill="black", align="center")
						
					if (status['gpioport2'] & 1 != 1):
						draw.text((63, 48), "VALET", font=font, fill="black", align="center")
					
					if (status['gpioport1'] & 32):
						draw.text((97, 48), "FAN", font=font, fill="black", align="center")
		else:
			queueLock.release()

dataQ = Queue.Queue(maxsize=1)
statusQ = Queue.Queue(maxsize=1)
queueLock = threading.Lock()
statusQ.put(status)

t1 = FuncThread(oledOut,dataQ,statusQ,dataTitle,fontFile,logoImage)
t1.daemon = True
t1.start()


# Open debug file for appending
debugfile = open(scriptdir + "/logs/mserial.log", 'a')
debugfile.write(datetime.now().strftime('%d-%m-%Y %H.%M.%S' + ": Starting...\n"))
debugfile.flush()

while (True):
	
	# Attempt to connect, unless exception occurs
	
	while (status['connected'] == 0 ):
		try: 
			ser.open()
		except Exception, e:
			time.sleep(1)
			break
		
		# Flush Serial Buffers
		ser.flushInput() 
		ser.flushOutput()
		
		# Request Signature
		ser.write("Q")
		time.sleep(0.4)
		response= ser.readline()

		# Close connection on empty response
		if(response == ""):
			ser.close()
		else:
			status['connected'] = 1
			statusQ.put(status)
	if (status['connected'] == 1):
		
		#Open long term fuel file
		
		ltt = open(scriptdir + "/ltt.csv","a+")

		read = ltt.read()
		ltc = {}
		if (read == ""):
			ltc['distance'] = 0.001
			ltc['fuelUsed'] = 0.00000001
		else:			
			read = read.split(csvchar)
			
			ltc['distance'] = float(read[0])
			ltc['fuelUsed'] = float(read[1])
				
		#Reset trip data		
		ecuData['Time'] = 0
		ecuData['TripDistance'] = 0.0000001
		ecuData['TripLitres'] = 0.0000001

	while ( status['connected'] == 1 ):
		try: 
			#Begin timer
			startTime = time.time()
			
			#Flush Serial I/O buffers
			ser.flushInput() 
			ser.flushOutput()

			#write data
			ser.write("A")
			
			#Give the controller time to respond
			time.sleep(0.2)
			
			#Read first 200 bytes
			response = ser.read(200)
			
			# Error trap response
			if(response == ""):
				status['connected'] = 0
				statusQ.put(status)
				ser.close()
				ltt.close()
				break
				
			#reset error status
			if (not status['lock']):
				status['error'] = 0
				
			
			# Convert response from controller to something we can work with
			data = response.encode('hex')
			data = re.findall('..',data)
			
			# Time the read from controller
			FrameTime = (time.time() - startTime)
	
			#Format returned data
        
			ecuData['Time'] += FrameTime
			ecuData['MAT'] = mserial.formatEcuData(data,20,21,-320,0.05555)
			ecuData['CLT'] = mserial.formatEcuData(data,22,23,-320,0.05555)
			ecuData['EGOCor'] = mserial.formatEcuData(data,34,35,0,0.1)
			ecuData['RPM'] = mserial.formatEcuData(data,6,7)
			ecuData['PW'] = mserial.formatEcuData(data,2,3,0,0.000666)
			ecuData['Speed'] = mserial.formatEcuData(data,104,105)
			ecuData['DutyCycle'] = mserial.calculateDutyCycle(ecuData['RPM'],ecuData['PW'])
			ecuData['AFR'] = mserial.formatEcuData(data,28,29,0,0.1)
			ecuData['TPS'] = mserial.formatEcuData(data,24,25,0,0.1)
			ecuData['MAP'] = mserial.formatEcuData(data,18,19,0,0.1)
			ecuData['Baro'] = mserial.formatEcuData(data,16,17,0,0.1)
			ecuData['Voltage'] = mserial.formatEcuData(data,26,27,0,0.1)
			ecuData['engine'] = int(data[11],16)
			ecuData['gpioport1'] = int(data[167],16)
			ecuData['gpioport2'] = int(data[168],16)
			calcDeadTime = ((ecuData['Voltage'] - 13.2) * DeadTimeCorrection) + DeadTime
			ecuData['LPH'] = mserial.calculateLph(ecuData['RPM'],injectorFlow,ecuData['PW'],calcDeadTime)
			ecuData['FrameDistance'] = ecuData['Speed'] / 3600 * FrameTime
			ecuData['TripDistance'] += ecuData['FrameDistance']
			ecuData['FrameLitres'] = ecuData['LPH'] / 3600 * FrameTime
			ecuData['TripLitres'] += ecuData['FrameLitres']
			
			# Calculate if we are in boost or vacuum
			ecuData['boostVac'] = ecuData['MAP'] - ecuData['Baro']
			if ( ecuData['MAP'] > ecuData['Baro'] ):
				ecuData['boostVac'] = ecuData['boostVac'] * 0.1450
				boostUnits = "psi"
			else:
				ecuData['boostVac'] = ecuData['boostVac'] * 0.2953007
				boostUnits = "in/hg"
			
						#Do we need to turn the buzzer on or off, and should be allow it to reactivate
			if ecuData['RPM'] > shiftRPM and buzzerStatus == 0:
				buzzer = GPIO.PWM(25,10)
				buzzer.start(40)
				lastBuzz = time.time()
				buzzerStatus = 1
			if time.time() - lastBuzz >= 0.5:
				buzzer.stop()
			if ecuData['RPM'] < (shiftRPM - shiftDrop):
				buzzerStatus = 0
				
																	 
			# Add distance travelled and fuel used this frame to long term data file
			ltc['distance'] += ecuData['FrameDistance']
			ltc['fuelUsed'] += ecuData['FrameLitres'] 
			ltc['average'] = (ltc['distance'] /  ltc['fuelUsed']) * 4.54
			lttwrite = str(ltc['distance']) + "," + str(ltc['fuelUsed'])
				
			#Calculate Instant MPG or display LPH if not moving
			if (ecuData['Speed'] == 0):
				ecuData['InstantMPG'] = 0
			elif (ecuData['LPH'] == 0):
				ecuData['InstantMPG'] = 0
			elif (ecuData['LPH'] < 0):
				ecuData['LPH'] = 0
				ecuData['InstantMPG'] = 99.9
			else:
				ecuData['InstantMPG'] = round(ecuData['Speed'] / (ecuData['LPH'] / 4.54),2)
				
			if (ecuData['InstantMPG'] > 99.9):
				ecuData['InstantMPG'] = 99.9
			
			# Write long term data to file
			ltt.seek(0)
			ltt.truncate()
			ltt.write(lttwrite)
            
			ltt.flush()	
			
			# Prep data for sending to display thread
			oledData = {}
			oledData = [str(round(ltc['average'],2)) + "mpg"]
			oledData += [str(round((ecuData['TripDistance'] /ecuData['TripLitres'] ) * 4.54,2)) + "mpg"] # Trip
			oledData += [str(round(ecuData['InstantMPG'],2)) + "mpg"] # Instant
			oledData += [str(round(ecuData["CLT"],1)) + degrees + "c"]
			oledData += [str(round(ecuData["MAT"],1)) + degrees + "c"]
			oledData += [str(round(ecuData["boostVac"],2)) + boostUnits]
			oledData += [str(round(ecuData["AFR"],1)) + ":1"]
			oledData += [str(round(ecuData["EGOCor"],1)) + "%"]
			oledData += [str(round(ecuData["DutyCycle"],2)) + "%"]
			oledData += [str(round(ecuData["TPS"],2)) + "%"]
			oledData += [str(round(ecuData["Voltage"],2)) + "v"]
			
			status['engine'] = ecuData['engine']
			status['gpioport1'] = ecuData['gpioport1']
			status['gpioport2'] = ecuData['gpioport2']
            
    
       			#Loop through limits, display error if sensor limit exceeded
			for d in limits:
				if (ecuData[d] > limits[d][0]):
					status['error'] = 1
					status['message'] = limits[d][1]
			
			# Send data to display thread
			statusQ.put(status)
			dataQ.put(oledData)
			
		# Catch any exceptions and throw generic error
		except Exception,e:
			print e
			debugfile.write(datetime.now().strftime('%d-%m-%Y %H.%M.%S' + ": Unknown Error...Waiting\n"))
			debugfile.flush()
			time.sleep(1)
			ser.close()
			status['connected'] = 0
			status['connected'] = 0
			statusQ.put(status)
			break

exitFlag = 1
debugfile.close()
GPIO.cleanup()