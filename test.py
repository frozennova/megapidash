from oled.serial import i2c
from oled.device import ssd1306, sh1106
from oled.render import canvas
from PIL import ImageFont, ImageDraw, Image

abspath = os.path.abspath(__file__)
scriptdir = os.path.dirname(abspath)
logoImage = scriptdir + "/mx5.png"
fontFile = scriptdir + "/DroidSans.ttf"

# Configure GPIO
GPIO.setmode(GPIO.BCM)

#Setup OLED
seriali2c = i2c(port=1, address=0x3C)
device = sh1106(seriali2c)    

with canvas(device) as draw:
	font = ImageFont.truetype(fontFile,14)

	size = font.getsize("Boost")
	draw.text(( 64 - (size[0] / 2), 5), "Boost"), font=font, fill="white", align="center")

	font = ImageFont.truetype(fontFile,22)
	size = font.getsize("11.2")
	draw.text((64-(size[0] / 2), 20), "11.2", font=font, fill="white", align="center")